import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class EnvioServiceProvider {

  // private _url = 'http://192.168.0.106:3000/api/v1';
  private _url = 'https://vocacional-api.herokuapp.com/api/v1'

  constructor(private _http: HttpClient) {
  }

  envia(respostas) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'X-User-Email':  localStorage.getItem("email"),
        'X-User-Token': localStorage.getItem("token")
    })};
    let payload = { user: {answers_attributes: respostas} };
    return this._http.patch(this._url+'/answers/update', payload, httpOptions);
  }

}
