import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Question } from '../../modelos/question';
import { Answer } from '../../modelos/answer';

@Injectable()
export class QuestionsServiceProvider {

  // private _url = 'http://192.168.0.106:3000/api/v1';
  private _url = 'https://vocacional-api.herokuapp.com/api/v1';

  constructor(private _http: HttpClient) {
  }

  lista() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'X-User-Email':  localStorage.getItem("email"),
        'X-User-Token': localStorage.getItem("token")
    })};
    return this._http.get<Answer[]>(this._url+'/answers/retrieve.json', httpOptions);
  }

}
