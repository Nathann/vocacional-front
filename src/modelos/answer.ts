export interface Answer {
    question_id: number;
    content: boolean;
    id: number;
    category: string;
}
