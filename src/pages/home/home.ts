import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { NavController, LoadingController, AlertController, ToastController, Alert, Slides } from 'ionic-angular';
import { Question } from '../../modelos/question';
import { Answer } from '../../modelos/answer';
import { ResultPage } from '../result/result';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { QuestionsServiceProvider } from '../../providers/questions-service/questions-service';
import { EnvioServiceProvider } from '../../providers/envio-service/envio-service';
import { NavLifecycles } from '../../utils/ionic/nav/nav-lifecycles';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements NavLifecycles {
  @ViewChild(Slides) slides: Slides;
  public answers: Answer[];
  public categories: any = [];
  public grouped_answers: any = {};
  public form: FormGroup;
  private _alerta: Alert;
  isLoggedIn: boolean = false;

  constructor(public navCtrl: NavController,
    private _loadingCtrl: LoadingController,
    private _alertCtrl: AlertController,
    private _toastCtrl: ToastController,
    private _questionsService: QuestionsServiceProvider,
    private _formBuilder: FormBuilder,
    private _envioService: EnvioServiceProvider,
    private _authService: AuthServiceProvider) {
      this.form = this._formBuilder.group({
        answers: []
      });
      if(!localStorage.getItem("token")) {
        this.navCtrl.setRoot(LoginPage);
      } else {
        this.isLoggedIn = true;
      }
    }

  ionViewDidLoad() {
    if (this.isLoggedIn) {
      let loading = this._loadingCtrl.create({
        content: 'Carregando perguntas...'
      });

      loading.present();

      this._questionsService.lista()
        .subscribe(
          (answers) => {
            this.answers = answers;
            loading.dismiss();
            const controls = this.answers.map(c => new FormControl(c.content));
            var all_categories = [];
            for (var i = 0; i < answers.length; i++) {
              var categoryName = answers[i].category;
              all_categories.push(categoryName);
              if (!this.grouped_answers[categoryName]) {
                this.grouped_answers[categoryName] = [];
              }
              this.grouped_answers[categoryName].push({id: i, answer: answers[i]});
            }

            this.categories = Array.from(new Set(all_categories));

            console.log(this.categories);

            this.form = this._formBuilder.group({
              answers: new FormArray(controls)
            })
          },
          (err: HttpErrorResponse) => {
            console.log(err);

            loading.dismiss();

            this._alertCtrl.create({
              title: 'Falha na conexão',
              subTitle: 'Não foi possível carregar a lista de perguntas. Tente novamente mais tarde!',
              buttons: [
                { text: 'Ok' }
              ]
            }).present();
          }
        );
    }
  }

  submit() {
    let loading = this._loadingCtrl.create({
      content: 'Enviando respostas...'
    });

    loading.present();
    const updated_answers = this.form.value.answers
      .map((v, i) => {
        let answer = this.answers[i];
        answer.content = v
        return answer
      })
    this._alerta = this._alertCtrl.create({
      title: 'Aviso',
      buttons: [
        {
          text: 'ok',
          handler: () => {
            this.navCtrl.setRoot(HomePage);
          }
        }
      ]
    });

    let mensagem = '';
    this._envioService.envia(updated_answers)
      .subscribe(
        (result) => this.navCtrl.push(ResultPage.name, {
          studyField: result
        }),
        () => mensagem = 'Ocorreu um erro! Tente novamente mais tarde!',
        () => {
            loading.dismiss();
        }
      );
  }

  logout() {
    let loading = this._loadingCtrl.create({
      content: 'Saindo...'
    });

    loading.present();
    this._authService.logout().then((result) => {
      loading.dismiss();
      this.navCtrl.setRoot(LoginPage);
    }, (err) => {
      loading.dismiss();
      this.presentToast(err);
    });
  }

  presentToast(msg) {
   let toast = this._toastCtrl.create({
     message: msg,
     duration: 3000,
     position: 'bottom',
     dismissOnPageChange: true
   });

   toast.onDidDismiss(() => {
     console.log('Dismissed toast');
   });

   toast.present();
 }

}
