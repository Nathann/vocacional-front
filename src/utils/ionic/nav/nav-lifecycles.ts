export interface NavLifecycles {
    ionViewDidLoad?(): void;
    ionViewCanEnter?(): void;
}
